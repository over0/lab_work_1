FROM python:3.10.4
WORKDIR /lab_work_1
COPY task1.py ./
COPY task2.py ./
COPY list.txt ./
RUN pip install python-dotenv
CMD ["python3", "./task1.py"]
CMD ["python3", "./task2.py"]