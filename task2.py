file = open('list.txt', mode='r', encoding='utf-8')
# В файле: первая цифра- цифра группы; 2-3 - последние цифры года рождения; 4-5 - месяц рождения; 6-7 - день рождения

people = []
nmbs = []


for row in file.readlines():
   n = int(float(row.split()[0]))
   people.append(' '.join(row.split()[1:]))
   nmbs.append(n)


max_years = nmbs.index(max(nmbs))
min_years = nmbs.index(min(nmbs))
oldest = people[max_years]
youngest = people[min_years]


print('Самый старший(ая): ' + oldest[8::1])
print('Самый младший(ая): ' + youngest[8::1])
file.close()